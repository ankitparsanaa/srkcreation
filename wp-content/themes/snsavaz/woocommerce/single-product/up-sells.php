<?php
/**
 * Single Product Up-Sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/up-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $upsells ) : 
	wp_enqueue_script('owlcarousel');
	?>

	<div class="upsells products">
		<div class="sns_products_heading">
			<h2><span><?php esc_html_e( 'Upsell products', 'snsavaz' ) ?></span></h2>
			<div class="navslider">
				<span class="prev"><i class="fa fa-long-arrow-left"></i></span>
				<span class="next"><i class="fa fa-long-arrow-right"></i></span>
			</div>
		</div>
		<?php woocommerce_product_loop_start(); ?>

			<?php foreach ( $upsells as $upsell ) : ?>

				<?php
				 	$post_object = get_post( $upsell->get_id() );

					setup_postdata( $GLOBALS['post'] =& $post_object );

					wc_get_template_part( 'content', 'product' ); ?>

			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery('.upsells ul').owlCarousel({
					items: 4,
					responsive : {
					    0 : { items: 1 },
					    480 : { items: 2 },
					    768 : { items: 3 },
					    992 : { items: 4 },
					    1200 : { items: 4 }
					},
					loop:true,
		            dots: false,
		            // animateOut: 'flipInY',
				    //animateIn: 'pulse',
				    autoplay: true,
		            onInitialized: callback,
		            slideSpeed : 800
				});
				function callback(event) {
		   			if(this._items.length > this.options.items){
				        jQuery('.upsells .navslider').show();
				        jQuery('.upsells').addClass('has-nav');
				    }else{
				        jQuery('.upsells .navslider').hide();
				        jQuery('.upsells').removeClass('has-nav');
				    }
				}
				jQuery('.upsells .navslider .prev').on('click', function(e){
					e.preventDefault();
					jQuery('.upsells ul').trigger('prev.owl.carousel');
				});
				jQuery('.upsells .navslider .next').on('click', function(e){
					e.preventDefault();
					jQuery('.upsells ul').trigger('next.owl.carousel');
				});
			});
		</script>

	</div>

<?php endif;

wp_reset_postdata();
