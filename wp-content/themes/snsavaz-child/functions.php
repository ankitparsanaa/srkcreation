<?php
/**
 * Enqueue style of child theme
 */
function snsavaz_child_enqueue_styles() {
    wp_enqueue_style( 'snsavaz-child-style', get_stylesheet_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'snsavaz_child_enqueue_styles', 100000 );

add_action('admin_init', 'my_general_section');  
function my_general_section() {  
    add_settings_section(  
        'my_settings_section', // Section ID 
        'Top Bar', // Section Title
        'my_section_options_callback', // Callback
        'general' // What Page?  This makes the section show up on the General Settings Page
    );

    add_settings_field( // Option 1
        'top_bar_tag_line', // Option ID
        'Tag Line', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'top_bar_tag_line' // Should match Option ID
        )  
    ); 

    register_setting('general','top_bar_tag_line', 'esc_attr');
}

function my_section_options_callback() { // Section Callback
}

function my_textbox_callback($args) {  // Textbox Callback
    $option = get_option($args[0]);
    echo '<input type="text" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" class="regular-text code" />';
}

function example_serif_font_and_large_address() {
   ?>
    <style> 
        #page {
           font-size: 1em;
        }

       .order-addresses address {
           font-size: 2.2em;
           line-height: 125%;
       }
    </style>
<?php
}
add_action( 'wcdn_head', 'example_serif_font_and_large_address', 20 );
?>