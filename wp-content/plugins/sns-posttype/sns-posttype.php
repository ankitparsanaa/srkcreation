<?php
/*
Plugin Name: SNS Posttype
Plugin URI: http://snstheme.com
Description: Define some postype for snstheme.com
Version: 1.0
Author URI: http://snstheme.com
License: GPL2+
*/

class SNS_Posttype{
	function __construct(){
		add_action('init', array($this,'sns_posttype_brand'));
		add_action('init', array($this,'sns_posttype_testimonial'));
	}
	function sns_posttype_brand(){
		$labels = array(
			'name' => __( 'Brand', 'snsnova' ),
			'singular_name' => __( 'Brand', 'snsnova' ),
			'add_new' => __( 'Add New Brand', 'snsnova' ),
			'add_new_item' => __( 'Add New Brand', 'snsnova' ),
			'edit_item' => __( 'Edit Brand', 'snsnova' ),
			'new_item' => __( 'New Brand', 'snsnova' ),
			'view_item' => __( 'View Brand', 'snsnova' ),
			'search_items' => __( 'Search Brands', 'snsnova' ),
			'not_found' => __( 'No Brands found', 'snsnova' ),
			'not_found_in_trash' => __( 'No Brands found in Trash', 'snsnova' ),
			'parent_item_colon' => __( 'Parent Brand:', 'snsnova' ),
			'menu_name' => __( 'Our Brands', 'snsnova' ),
		);

		$args = array(
		    'labels' => $labels,
		    'hierarchical' => true,
		    'description' => 'List Brands',
		    'supports' => array( 'title', 'thumbnail' ),
		    'public' => false,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'menu_position' => 5,
		    'show_in_nav_menus' => true,
		    'publicly_queryable' => true,
		    'exclude_from_search' => false,
		    'has_archive' => true,
		    'query_var' => true,
		    'can_export' => true,
		    'rewrite' => true,
		    'capability_type' => 'post'
		);
		register_post_type( 'brand', $args );
	}
	function sns_posttype_testimonial(){
		$labels = array(
			'name' => __( 'Testimonial', 'snsnova' ),
			'singular_name' => __( 'Testimonial', 'snsnova' ),
			'add_new' => __( 'Add New Testimonial', 'snsnova' ),
			'add_new_item' => __( 'Add New Testimonial', 'snsnova' ),
			'edit_item' => __( 'Edit Testimonial', 'snsnova' ),
			'new_item' => __( 'New Testimonial', 'snsnova' ),
			'view_item' => __( 'View Testimonial', 'snsnova' ),
			'search_items' => __( 'Search Testimonial', 'snsnova' ),
			'not_found' => __( 'No Testimonial found', 'snsnova' ),
			'not_found_in_trash' => __( 'No Testimonial found in Trash', 'snsnova' ),
			'parent_item_colon' => __( 'Parent Testimonial:', 'snsnova' ),
			'menu_name' => __( 'Testimonial', 'snsnova' ),
		);

		$args = array(
		    'labels' => $labels,
		    'hierarchical' => true,
		    'description' => 'List Brands',
		    'supports' => array( 'title', 'editor', 'thumbnail'),
		    'public' => false,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'menu_position' => 6,
		    'show_in_nav_menus' => true,
		    'publicly_queryable' => true,
		    'exclude_from_search' => false,
		    'has_archive' => true,
		    'query_var' => true,
		    'can_export' => true,
		    'rewrite' => true,
		    'capability_type' => 'post'
		);
		register_post_type( 'testimonial', $args );
	}  	
}
function snsposttype_load(){
	global $snsposttype;
	$snsposttype = new SNS_Posttype();
}
add_action( 'plugins_loaded', 'snsposttype_load' );
?>